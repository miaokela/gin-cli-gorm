package controller

import (
	"backend/dao/mysql"
	"backend/models"
	"backend/utils/api"
	"github.com/gin-gonic/gin"
	"strconv"
)

// IndexView 操作历史列表接口
func IndexView(c *gin.Context) {
	api.ResponseSuccess(c, gin.H{})
}

// OperateHistoryList 操作历史列表接口
func OperateHistoryList(c *gin.Context) {
	var operateHistoryList []models.OperateHistory

	page := 1
	pageSize := 10

	pageParam := c.Query("page")
	pageSizeParam := c.Query("page_size")
	if pageParam != "" {
		page, _ = strconv.Atoi(pageParam)
	}
	if pageSizeParam != "" {
		pageSize, _ = strconv.Atoi(pageSizeParam)
	}
	data, rows, err := mysql.ListOperateHistory(operateHistoryList, page, pageSize)
	if err != nil {
		api.ResponseError(c, api.CodeServerBusy)
		return
	}
	api.ResponseSuccess(c, gin.H{
		"total": rows,
		"data":  data,
	})
}

// OperateHistoryCreate 操作历史创建接口
func OperateHistoryCreate(c *gin.Context) {
	var operateHistory models.OperateHistory
	if err := c.ShouldBind(&operateHistory); err != nil {
		api.ResponseError(c, api.CodeInvalidParams)
		return
	}
	cid, err := mysql.CreateOperateHistory(operateHistory)
	if err != nil {
		api.ResponseError(c, api.CodeServerBusy)
		return
	}
	api.ResponseSuccess(c, gin.H{
		"id": cid,
	})
}

// OperateHistoryUpdate 操作历史更新接口
func OperateHistoryUpdate(c *gin.Context) {
	var operateHistory models.OperateHistory
	if err := c.ShouldBind(&operateHistory); err != nil {
		api.ResponseError(c, api.CodeInvalidParams)
		return
	}
	cid, _ := strconv.ParseUint(c.Param("id"), 10, 64)
	cid, err := mysql.UpdateOperateHistory(operateHistory, cid)
	if err != nil {
		api.ResponseError(c, api.CodeServerBusy)
		return
	}
	api.ResponseSuccess(c, gin.H{
		"id": cid,
	})
}

// OperateHistoryDelete 操作历史删除接口
func OperateHistoryDelete(c *gin.Context) {
	id, _ := strconv.ParseUint(c.Param("id"), 10, 64)
	cid, err := mysql.DeleteOperateHistory(id)
	if err != nil {
		api.ResponseError(c, api.CodeServerBusy)
		return
	}
	api.ResponseSuccess(c, gin.H{
		"id": cid,
	})
}
