package mysql

import (
	"backend/models"
	"fmt"
)

func ListOperateHistory(datas []models.OperateHistory, page int, pageSize int) ([]models.OperateHistory, int64, error) {
	offset := (page - 1) * pageSize
	result := Orm.Order("id desc").Offset(offset).Limit(pageSize).Find(&datas)
	return datas, result.RowsAffected, result.Error
}

func CreateOperateHistory(data models.OperateHistory) (uint64, error) {
	fmt.Printf("%#v", data)
	result := Orm.Create(&data)
	return data.ID, result.Error
}

func FindOperateHistory(id uint64) (models.OperateHistory, error) {
	var model models.OperateHistory
	result := Orm.First(&model, id)
	return model, result.Error
}

func UpdateOperateHistory(data models.OperateHistory, id uint64) (uint64, error) {
	var model models.OperateHistory
	row := Orm.First(&model, id)
	if row.Error == nil {
		result := Orm.Model(&model).Updates(&data)
		return model.ID, result.Error
	}
	return 0, row.Error
}

func DeleteOperateHistory(id uint64) (int64, error) {
	var model models.OperateHistory
	result := Orm.Delete(&model, id)
	return result.RowsAffected, result.Error
}
