package mysql

import (
	"backend/models"
	"backend/settings"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var Orm *gorm.DB

// InitORM 初始化MySQL连接
func InitORM(cfg *settings.MySQLConfig) (err error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.DB)
	Orm, err = gorm.Open("mysql", dsn)
	if err != nil {
		return
	}
	Orm.AutoMigrate(&models.OperateHistory{}, &models.User{})
	return
}

// CloseORM 关闭MySQL连接
func CloseORM() {
	_ = Orm.Close()
}
