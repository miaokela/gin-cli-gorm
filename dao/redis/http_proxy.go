package redis

// GetHttpProxy 获取已经使用的代理列表
func GetHttpProxy() []string {
	es, _ := client.SMembers(UsedHttpProxyKey).Result()
	return es
}
