package models

import "time"

type BaseModel struct {
	ID        uint64     `gorm:"primary_key"`
	CreatedAt time.Time  `gorm:"comment:'创建时间';type:datetime;"`
	UpdatedAt time.Time  `gorm:"comment:'修改时间';type:datetime;"`
	DeletedAt *time.Time `sql:"index"`
}
