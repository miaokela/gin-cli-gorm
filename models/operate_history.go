package models

type OperateHistory struct {
	BaseModel
	PnodeId *uint64 `json:"pnode_id" db:"pnode_id"`
	Content string  `json:"content" db:"content" binding:"required,min=2,max=20"`
}
