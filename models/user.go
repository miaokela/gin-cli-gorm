package models

type User struct {
	BaseModel
	UserID   uint64 `json:"user_id" db:"user_id"`
	Username string `json:"username" db:"username"`
	Password string `json:"password" db:"password"`
}
