package routers

import (
	"backend/controller"
	"backend/middlewares"
	"net/http"

	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	v1 := r.Group("/api/v1")
	// 用户注册
	v1.POST("/register", controller.SignUpHandler)
	// 用户登录
	v1.POST("/login", controller.LoginHandler)
	// 刷新token
	v1.POST("/refresh_token", controller.RefreshTokenHandler)

	v1.Use(middlewares.JWTAuthMiddleware()) // 注册JWT中间件
	{
		v1.GET("/index", controller.IndexView)
		v1.GET("/op_history", controller.OperateHistoryList)
		v1.POST("/op_history", controller.OperateHistoryCreate)
		v1.PUT("/op_history/:id", controller.OperateHistoryUpdate)
		v1.DELETE("/op_history/:id", controller.OperateHistoryUpdate)
	}

	r.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "404",
		})
	})
	return r
}
