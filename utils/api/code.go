package api

type MyCode int64

const (
	CodeSuccess         MyCode = 1000 // 成功响应
	CodeInvalidParams   MyCode = 1001 // 参数检验失败
	CodeUserExist       MyCode = 1002 // 用户已存在
	CodeUserNotExist    MyCode = 1003 // 用户不存在
	CodeInvalidPassword MyCode = 1004 // 密码错误
	CodeServerBusy      MyCode = 1005 // 请求超时

	CodeInvalidToken      MyCode = 1006 // Token无效
	CodeInvalidAuthFormat MyCode = 1007 // 认证失败
	CodeNotLogin          MyCode = 1008 // 用户未登录
)

var msgFlags = map[MyCode]string{
	CodeSuccess:         "success",
	CodeInvalidParams:   "请求参数错误",
	CodeUserExist:       "用户名重复",
	CodeUserNotExist:    "用户不存在",
	CodeInvalidPassword: "用户名或密码错误",
	CodeServerBusy:      "服务繁忙",

	CodeInvalidToken:      "无效的Token",
	CodeInvalidAuthFormat: "认证格式有误",
	CodeNotLogin:          "未登录",
}

func (c MyCode) Msg() string {
	msg, ok := msgFlags[c]
	if ok {
		return msg
	}
	return msgFlags[CodeServerBusy]
}
