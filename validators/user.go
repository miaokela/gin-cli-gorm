package validators

import (
	"encoding/json"
	"errors"
)

// RegisterForm 用户注册的表单校验
type RegisterForm struct {
	Username        string `json:"username" db:"username"`
	Password        string `json:"password" db:"password"`
	ConfirmPassword string `json:"confirm_password" db:"confirm_password"`
}

// LoginForm 用户登录的表单校验
type LoginForm struct {
	Username string `json:"username" db:"username"`
	Password string `json:"password" db:"password"`
	UserId   uint64 `json:"user_id" db:"user_id"`
}

// UnmarshalJSON 实现解析json数据接口
func (r *RegisterForm) UnmarshalJSON(data []byte) (err error) {
	required := struct {
		UserName        string `json:"username"`
		Password        string `json:"password"`
		ConfirmPassword string `json:"confirm_password"`
	}{}
	err = json.Unmarshal(data, &required)
	if err != nil {
		return
	} else if len(required.UserName) == 0 {
		err = errors.New("缺少必填字段username")
	} else if len(required.Password) == 0 {
		err = errors.New("缺少必填字段password")
	} else if required.Password != required.ConfirmPassword {
		err = errors.New("两次密码不一致")
	} else {
		r.Username = required.UserName
		r.Password = required.Password
		r.ConfirmPassword = required.ConfirmPassword
	}
	return
}

func (u *LoginForm) UnmarshalJSON(data []byte) (err error) {
	required := struct {
		UserName string `json:"username" db:"username"`
		Password string `json:"password" db:"password"`
	}{}
	err = json.Unmarshal(data, &required)
	if err != nil {
		return
	} else if len(required.UserName) == 0 {
		err = errors.New("缺少必填字段username")
	} else if len(required.Password) == 0 {
		err = errors.New("缺少必填字段password")
	} else {
		u.Username = required.UserName
		u.Password = required.Password
	}
	return
}
